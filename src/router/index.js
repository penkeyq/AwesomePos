import Vue from 'vue'
import VueRouter from 'vue-router'
import Pos from '../components/page/Pos.vue'
Vue.use(VueRouter)

let routes =[{
    path:'/',
    component:Pos
}];
let router = new VueRouter({
    routes
})

export default router